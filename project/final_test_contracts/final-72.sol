pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address owner;

  function check_rec(uint i, address a, address b) public returns (bool) {
    if (i == 0) {
      return a == b;
    } else {
      return check_rec(i-1, a, b);
    }
  }

  function foo(uint x) public {
    bool b = check_rec(x, msg.sender, owner);
    require(b); // not a guard
    // The return value of check_rec(x, msg.sender, owner) implicitly depends on x (untrusted).
    selfdestruct(msg.sender); // vulnerable
  }
}