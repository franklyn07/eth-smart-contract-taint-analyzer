pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address payable owner;
  int safe_int;

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == owner); // guard
  }

  function set() public returns(address payable) {
    if (0 > 0) {
      return msg.sender;
    } else {
      return address(0xAAAAAAAA);
    }
  }

  function foo(address payable a) public {
    address payable x = set(); // If the execution goes through the else branch at line 17, x does not depend on msg.sender, the require statement at line 24 is not a guard, and a is an untrusted address.
    require(x == x); // not a guard
    selfdestruct(a); // vulnerable
  }
}