pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  int y;
  address payable owner;
  address payable admin;

  function foo(int x) public {
    y = x + x; // y becomes trusted after seeing the guard.
    require(msg.sender == owner); // guard
  }

  function bar() public {
    bool b = (msg.sender == owner);
    if (b) { // guard
        selfdestruct(msg.sender); // safe
    } else {
        // ...
    }
    selfdestruct(msg.sender); // safe
  }
}
