pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address owner;
  address admin;
  int safe_int;

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == owner); // guard
  }

  function foo(int x) public returns(bool) {
    if (x > 0) {
      return msg.sender == owner;
    } else {
      return msg.sender == admin;
    }
  }

  function bar(int x) public {
    bool b = foo(safe_int);
    require(b); // guard
    // The return value b of foo(x) at line 24 depends on msg.sender and all other values it depends on are trusted.
    selfdestruct(msg.sender); // safe
  }
}