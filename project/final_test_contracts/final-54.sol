pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address payable user;
  address payable owner;
  int safe_int;

  function registerUser(address payable a) public {
    user = a;
  }

  function kill(address payable a) public {
    require(msg.sender == user); // Not a guard because user is untrusted if registerUser is called before.
    if (safe_int > 0) {
      registerUser(owner); // This call makes user trusted.
    } // If the if branch is not taken, user can still be untrusted.
    require(msg.sender == user); // not a guard
    selfdestruct(a); // vulnerable
  }
}
