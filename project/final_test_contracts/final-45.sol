pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address payable owner;
  int safe_int;

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == owner); // guard
  }

  function bar() public returns(address payable) {
    if (safe_int > 0) {
      return msg.sender;
    } else {
      return address(0xAAAAAAAA);
    }
  }

  function foo() public {
    address payable x = bar(); // If the execution goes through the if branch at line 15, x depends on msg.sender (untrusted).
    selfdestruct(x); // vulnerable
  }
}