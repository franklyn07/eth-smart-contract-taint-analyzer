pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address safe_addr;
  address unsafe_addr;
  address a;
  address b;
  int safe_int;

  function set_unsafe_addr(address z) public {
    unsafe_addr = z;
  }

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == safe_addr); // guard
  }

  function foo() public {
    if (safe_int > 0) {
      a = safe_addr;
      b = unsafe_addr;
    } else {
      a = unsafe_addr;
      b = safe_addr;
    }
  }

  // If foo() is called before calling bar(), either a or b is trusted.
  // If foo() is never called before calling bar(), both a and b are trusted.
  // Therefore, one of the require statements at line 35 and 36 is a guard, making z trusted.
  function bar(address payable z) public {
    require(msg.sender == a);
    require(msg.sender == b);
    selfdestruct(z); // safe
  }
}