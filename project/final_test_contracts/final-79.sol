pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address owner;

  function check_int(int i) public returns(bool) {
    return i == 1;
  }

  function bar(int i) public {
    bool b = (msg.sender == owner || check_int(100));
    require(b); // guard
    selfdestruct(msg.sender); // safe
  }
}
