pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address owner;
  address admin;

  function foo(int x) public returns(bool) {
    if (x > 0) {
      return msg.sender == owner;
    } else {
      return msg.sender == admin;
    }
  }

  function bar(int x) public {
    bool b = foo(x);
    require(b); // not a guard
    // The return value b of foo(x) at line 18 implicitly depends on x (untrusted).
    selfdestruct(msg.sender); // vulnerable
  }
}