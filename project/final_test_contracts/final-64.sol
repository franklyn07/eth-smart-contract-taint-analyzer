pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address owner;
  address admin;

  function foo(int x) public returns(bool) {
    bool b;
    if (x > 0) {
      b = msg.sender == owner;
    } else {
      b = msg.sender == admin;
    }
    return b;
  }

  function bar(int x) public {
    bool b = foo(x);
    require(b); // not a guard
    // The return value b of foo(x) at line 20 implicitly depends on x (untrusted).
    selfdestruct(msg.sender); // vulnerable
  }
}