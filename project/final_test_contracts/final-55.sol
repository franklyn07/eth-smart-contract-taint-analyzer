pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address payable user;
  address payable owner;
  int safe_int;

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == owner); // guard
  }

  function registerUser(address payable a) public {
    user = a;
  }

  function kill(address payable a) public {
    require(msg.sender == user); // Not a guard because user is untrusted if registerUser is called before.
    if (safe_int > 0) {
      registerUser(owner); // This call makes user trusted.
    } else {
      require(msg.sender == owner); // guard
    }
    require(msg.sender == user); // not a guard
    selfdestruct(a); // safe 
    // If the execution goes through the if branch at line 21, user becomes trusted and the require statement at line 26 is a guard.
    // If the execution goes through the else branch at line 13, it goes through the guard at line 24.
  }
}
