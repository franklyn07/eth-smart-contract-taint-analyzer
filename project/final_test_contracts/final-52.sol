pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address owner;
  address user;

  function set_user_with_check(address a, address b) public {
    user = a;
    require(check_address(b));
  }

  function check_address(address a) public returns(bool) {
    return msg.sender == a;
  }

  function foo(address a, address b) public {
    set_user_with_check(a, owner);
    require(msg.sender == user); // guard because line 15 is a guard during the execution of function foo, making a (and thus user) trusted.
    selfdestruct(msg.sender); // safe
  }
}
