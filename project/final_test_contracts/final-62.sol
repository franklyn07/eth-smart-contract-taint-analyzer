pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address owner;
  int safe_int;

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == owner); // guard
  }

  function foo() public {
    if (safe_int > 0) {
      selfdestruct(msg.sender); // vulnerable
      // selfdestruct immediately terminates the transaction. Therefore, when the if branch at line 15 is taken, the guard at line 19 is not executed.
    }
    require(msg.sender == owner); // guard
  }
}