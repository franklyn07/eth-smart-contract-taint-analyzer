pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
	address payable user;
	address payable owner;
  int x;

  function set_x(int i) public {
    x = i;
    require(msg.sender == owner); // guard
  }
	
	function registerUser() public {
		user = msg.sender;
	}

	function main() public {
		if (msg.sender == user) { // not a guard
			if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){
			if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){
			if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){if(x>5){
				selfdestruct(msg.sender); // vulnerable
			}}}}}}}}}}}}
			}}}}}}}}}}}}
			}}}}}}}}}}}}
		} 
	}
}
