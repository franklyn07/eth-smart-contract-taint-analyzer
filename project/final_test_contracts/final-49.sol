pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address safe_addr;
  address unsafe_addr;
  address a;
  address b;
  address c;
  int safe_int;

  function set_unsafe_addr(address z) public {
    unsafe_addr = z;
  }

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == safe_addr); // guard
  }

  function foo1() public {
    a = safe_addr;
    b = unsafe_addr;
    c = unsafe_addr;
  }

  function foo2() public {
    a = unsafe_addr;
    b = safe_addr;
    c = unsafe_addr;
  }

  function foo3() public {
    a = unsafe_addr;
    b = unsafe_addr;
    c = safe_addr;
  }

  // Similar to 48.sol, one of the require statements at line 42-44 is a guard, making z trusted.
  function bar(address payable z) public {
    require(msg.sender == a);
    require(msg.sender == b);
    require(msg.sender == c);
    selfdestruct(z); // safe
  }
}