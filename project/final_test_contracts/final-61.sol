pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
	address payable user;
	address payable owner;

	function registerUser() public {
		user = msg.sender;
	}

	function check(address x) public returns(address) { return foo1(x); }
	function foo1(address x) public returns(address) { return foo2(x); }
	function foo2(address x) public returns(address) { return foo3(x); }
	function foo3(address x) public returns(address) { return foo4(x); }
	function foo4(address x) public returns(address) { return foo5(x); }
	function foo5(address x) public returns(address) { return foo6(x); }
	function foo6(address x) public returns(address) { return foo7(x); }
	function foo7(address x) public returns(address) { return foo8(x); }
	function foo8(address x) public returns(address) { return foo9(x); }
	function foo9(address x) public returns(address) { return foo10(x); }
	function foo10(address x) public returns(address) { return foo11(x); }
	function foo11(address x) public returns(address) { return foo12(x); }
	function foo12(address x) public returns(address) { return foo13(x); }
	function foo13(address x) public returns(address) { return foo14(x); }
	function foo14(address x) public returns(address) { return foo15(x); }
	function foo15(address x) public returns(address) { return foo16(x); }
	function foo16(address x) public returns(address) { return foo17(x); }
	function foo17(address x) public returns(address) { return foo18(x); }
	function foo18(address x) public returns(address) { return foo19(x); }
	function foo19(address x) public returns(address) { return foo20(x); }
	function foo20(address x) public returns(address) { return foo21(x); }
	function foo21(address x) public returns(address) { return foo22(x); }
	function foo22(address x) public returns(address) { return foo23(x); }
	function foo23(address x) public returns(address) { return foo24(x); }
	function foo24(address x) public returns(address) { return foo25(x); }
	function foo25(address x) public returns(address) { return foo26(x); }
	function foo26(address x) public returns(address) { return foo27(x); }
	function foo27(address x) public returns(address) { return foo28(x); }
	function foo28(address x) public returns(address) { return foo29(x); }
	function foo29(address x) public returns(address) { return foo30(x); }
	function foo30(address x) public returns(address) { return foo31(x); }
	function foo31(address x) public returns(address) { return foo32(x); }
	function foo32(address x) public returns(address) { return foo33(x); }
	function foo33(address x) public returns(address) { return foo34(x); }
	function foo34(address x) public returns(address) { return foo35(x); }
	function foo35(address x) public returns(address) { return foo36(x); }
	function foo36(address x) public returns(address) { return foo37(x); }
	function foo37(address x) public returns(address) { return foo38(x); }
	function foo38(address x) public returns(address) { return foo39(x); }
	function foo39(address x) public returns(address) { return foo40(x); }
	function foo40(address x) public returns(address) { return foo41(x); }
	function foo41(address x) public returns(address) { return foo42(x); }
	function foo42(address x) public returns(address) { return foo43(x); }
	function foo43(address x) public returns(address) { return foo44(x); }
	function foo44(address x) public returns(address) { return foo45(x); }
	function foo45(address x) public returns(address) { return foo46(x); }
	function foo46(address x) public returns(address) { return foo47(x); }
	function foo47(address x) public returns(address) { return foo48(x); }
	function foo48(address x) public returns(address) { return foo49(x); }
	function foo49(address x) public returns(address) { return foo50(x); }
	function foo50(address x) public returns(address) { return foo51(x); }
	function foo51(address x) public returns(address) { return foo52(x); }
	function foo52(address x) public returns(address) { return foo53(x); }
	function foo53(address x) public returns(address) { return foo54(x); }
	function foo54(address x) public returns(address) { return foo55(x); }
	function foo55(address x) public returns(address) { return foo56(x); }
	function foo56(address x) public returns(address) { return foo57(x); }
	function foo57(address x) public returns(address) { return foo58(x); }
	function foo58(address x) public returns(address) { return foo59(x); }
	function foo59(address x) public returns(address) { return foo60(x); }
	function foo60(address x) public returns(address) { return foo61(x); }
	function foo61(address x) public returns(address) { return foo62(x); }
	function foo62(address x) public returns(address) { return foo63(x); }
	function foo63(address x) public returns(address) { return foo64(x); }
	function foo64(address x) public returns(address) { return foo65(x); }
	function foo65(address x) public returns(address) { return foo66(x); }
	function foo66(address x) public returns(address) { return foo67(x); }
	function foo67(address x) public returns(address) { return foo68(x); }
	function foo68(address x) public returns(address) { return foo69(x); }
	function foo69(address x) public returns(address) { return foo70(x); }
	function foo70(address x) public returns(address) { return foo71(x); }
	function foo71(address x) public returns(address) { return foo72(x); }
	function foo72(address x) public returns(address) { return foo73(x); }
	function foo73(address x) public returns(address) { return foo74(x); }
	function foo74(address x) public returns(address) { return foo75(x); }
	function foo75(address x) public returns(address) { return foo76(x); }
	function foo76(address x) public returns(address) { return foo77(x); }
	function foo77(address x) public returns(address) { return foo78(x); }
	function foo78(address x) public returns(address) { return foo79(x); }
	function foo79(address x) public returns(address) { return foo80(x); }
	function foo80(address x) public returns(address) { return foo81(x); }
	function foo81(address x) public returns(address) { return foo82(x); }
	function foo82(address x) public returns(address) { return foo83(x); }
	function foo83(address x) public returns(address) { return foo84(x); }
	function foo84(address x) public returns(address) { return foo85(x); }
	function foo85(address x) public returns(address) { return foo86(x); }
	function foo86(address x) public returns(address) { return foo87(x); }
	function foo87(address x) public returns(address) { return foo88(x); }
	function foo88(address x) public returns(address) { return foo89(x); }
	function foo89(address x) public returns(address) { return foo90(x); }
	function foo90(address x) public returns(address) { return foo91(x); }
	function foo91(address x) public returns(address) { return foo92(x); }
	function foo92(address x) public returns(address) { return foo93(x); }
	function foo93(address x) public returns(address) { return foo94(x); }
	function foo94(address x) public returns(address) { return foo95(x); }
	function foo95(address x) public returns(address) { return foo96(x); }
	function foo96(address x) public returns(address) { return foo97(x); }
	function foo97(address x) public returns(address) { return foo98(x); }
	function foo98(address x) public returns(address) { return foo99(x); }
	function foo99(address x) public returns(address) { return foo100(x); }
	function foo100(address x) public returns(address) { return x; }

	function main() public {
		if(msg.sender == check(owner)) { // guard
			selfdestruct(msg.sender);  // safe
		} 
	}

	function bar() public {
		if(msg.sender == check(user)) { // not a guard
			selfdestruct(msg.sender);  // vulnerable
		} 
	}

}
