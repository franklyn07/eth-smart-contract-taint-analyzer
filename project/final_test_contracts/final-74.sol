pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address owner;
  address a;
  address b;
  address c;

  function check(address x) public returns(bool) {
    return (msg.sender == x);
  }

  function foo_a() public {
    require(check(owner)); // guard
    a = msg.sender;
  }

  function foo_b() public {
    require(check(owner)); // guard
    b = msg.sender;
  }

  function foo_c() public {
    c = msg.sender;
  }

  function bar() public {
    require((check(a) || check(b)) && check(c)); // not a guard as c can be untrusted.
    selfdestruct(msg.sender); // vulnerable
  }
}
