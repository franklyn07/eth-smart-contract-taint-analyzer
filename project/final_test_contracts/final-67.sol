pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address owner;
  address admin;
  int safe_int;
  int unsafe_int;

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == owner); // guard
  }

  function set_unsafe_int(int i) public {
    unsafe_int = i;
  }

  function foo(int x) public returns(bool) {
    require(msg.sender == owner); // guard, making x trusted
    if (x > 0) {
      unsafe_int = safe_int * 2;
      if (unsafe_int > 0) {
        return msg.sender == owner;
      } else {
        return msg.sender == admin;
      }
    } else {
      return msg.sender == msg.sender;
    }
  }

  function bar(int x) public {
    bool b = foo(x);
    require(b); // guard
    // The return value b of foo(x) at line 35 depends on msg.sender and all other values it depends on are trusted.
    selfdestruct(msg.sender); // safe
  }
}