pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address owner;
  address a;
  address b;

  function check(address x) public returns(bool) {
    return (msg.sender == x);
  }

  function foo_a() public {
    require(check(owner)); // guard
    a = msg.sender;
  }

  function foo_b() public {
    require(check(owner)); // guard
    b = msg.sender;
  }

  function bar() public {
    require(check(a) && check(b)); // guard
    selfdestruct(msg.sender); // safe
  }
}
