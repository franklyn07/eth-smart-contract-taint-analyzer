pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address owner;
  address admin;
  int safe_int;
  int unsafe_int;

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == owner); // guard
  }

  function set_unsafe_int(int i) public {
    unsafe_int = i;
  }

  function foo(int x) public returns(bool) {
    if (unsafe_int > 0) {
      require(msg.sender == owner); // guard, making x trusted
      if (x > 0) {
        return msg.sender == owner;
      } else {
        return msg.sender == admin;
      }
    } else {
      return msg.sender == msg.sender;
    }
  }

  function bar(int x) public {
    bool b = foo(x);
    require(b); // not a guard
    // The return value b of foo(x) at line 34 depends on unsafe_int (untrusted).
    selfdestruct(msg.sender); // vulnerable
  }
}