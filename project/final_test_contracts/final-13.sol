pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  function unused_func () public returns(uint) {return 1;}

  function foo(int i) public {
    if (i > 5) {
      require(msg.sender == address(0xDEADBEEF)); // guard
      selfdestruct(msg.sender); // safe
    }
  }
}
