pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address owner;

  function check_int(int i) public returns(bool) {
    return i == 1;
  }

  function bar(int i) public {
    bool b = (msg.sender == owner || check_int(i));
    require(b); // not a guard
    selfdestruct(msg.sender); // vulnerable
  }
}
