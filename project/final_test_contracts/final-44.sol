pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address payable owner;
  int safe_int;

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == owner); // guard
  }

  function bar(address payable a) public returns(address payable) {
    if (safe_int > 0) {
      return a;
    } else {
      return address(0xAAAAAAAA);
    }
  }

  function foo(address payable a) public {
    address payable x = bar(a); // If the execution goes through the if branch at line 15, x depends on a (untrusted).
    selfdestruct(x); // vulnerable
  }
}
