pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address payable user;
  address payable owner;

  function registerUser(address payable a) public {
    user = a;
  }

  function kill(address payable a) public {
    require(msg.sender == user); // Not a guard because user is untrusted if registerUser is called before.
    registerUser(owner); // This call makes user trusted.
    require(msg.sender == user); // guard
    selfdestruct(a); // safe
  }
}
