pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  int y;
  address payable owner;
  address payable admin;

  function foo(int x) public {
    require(msg.sender == owner); // guard
    y = x + x;  // y remains trusted.
  }

  function bar() public {
    bool b = (msg.sender == owner || y < 10);
    if (b) { // guard
      selfdestruct(msg.sender); // safe
    } else { // guard
      selfdestruct(msg.sender); // safe
    }
  }
}
