pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address payable owner;
  int safe_int;

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == owner); // guard
  }

  function bar() public returns(address payable) {
    if (safe_int > 0) {
      return msg.sender;
    } else {
      return address(0xAAAAAAAA);
    }
  }

  function foo() public {
    address payable x = bar();
    // If the execution goes through the if branch at line 15, x depends on safe_int and msg.sender. The require statement at line 26 is a guard, making msg.sender (and thus x) trusted.
    // If the execution goes through the else branch at line 17, x depends on safe_int and constant 0xAAAAAAAA. Both are trusted values, so x is trusted. Note that the require statement at line 26 is not a guard in this case.
    require(x == x); // not a guard
    selfdestruct(x); // safe
  }
}