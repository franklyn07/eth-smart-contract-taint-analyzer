pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address owner;
  address admin;

  function foo(int x) public returns(bool) {
    require(msg.sender == owner); // guard, making x trusted
    if (x > 0) {
      return msg.sender == owner;
    } else {
      return msg.sender == admin;
    }
  }

  function bar(int x) public {
    bool b = foo(x);
    require(b); // guard
    // The return value b of foo(x) at line 19 depends on msg.sender and all other values it depends on are trusted.
    selfdestruct(msg.sender); // safe
  }
}