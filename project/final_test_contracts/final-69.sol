pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address owner;
  address admin;
  int safe_int;

  function set_safe_int(int i) public {
    safe_int = i;
    require(msg.sender == owner); // guard
  }

  function foo(int x) public returns(bool) {
    if (x > 0) {
      return msg.sender == owner;
    } else {
      return msg.sender == admin;
    }
  }

  function bar(int x) public {
    int y;
    if (x > 0) {
      y = safe_int;
    } else {
      y = 10;
    }
    bool b = foo(y);
    require(b); // not guard
    // The return value b of foo(y) at line 30 implicitly depends on x (untrusted).
    selfdestruct(msg.sender); // vulnerable
  }
}