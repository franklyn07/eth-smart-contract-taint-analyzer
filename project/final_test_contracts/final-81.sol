pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address payable a;

  function get() public returns (address payable) {
    address payable y = msg.sender;
    require(msg.sender == a); // guard
    return y;
  }

  function foo() public {
    selfdestruct(get()); // safe
  }
}
