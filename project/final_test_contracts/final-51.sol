pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address owner;
  address user;

  function set_user_with_check(address a, address b) public {
    user = a;
    require(check_address(b));
  }

  function check_address(address a) public returns(bool) {
    return msg.sender == a;
  }

  function foo(address a, address b) public {
    set_user_with_check(a, b);
    require(msg.sender == user); // not a guard because a and b are untrusted
    selfdestruct(msg.sender); // vulnerable
  }
}
