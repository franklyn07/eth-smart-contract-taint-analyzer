pragma solidity ^0.5.0;

// the contract is vulnerable
// the output of your analyzer should be Tainted
contract Contract {
  address payable owner;

  function bar(int z) public {
    address payable test = address(0xDEADBEEF);
    if (z >= 42) {
      test = msg.sender;
    } else {
      test = owner;
    }
    require(test == owner); // not a guard
    selfdestruct(msg.sender); // vulnerable
  }
}
