pragma solidity ^0.5.0;

// the contract is safe
// the output of your analyzer should be Safe
contract Contract {
  address owner;

  function check_rec(uint i, address a, address b) public returns (bool) {
    if (i == 0) {
      return a == b;
    } else {
      return check_rec(i-1, a, b);
    }
  }

  function foo() public {
    bool b = check_rec(10, msg.sender, owner);
    require(b); // guard
    selfdestruct(msg.sender); // safe
  }
}