pragma solidity ^0.5.0;

contract Contract {
  address owner;
  function bar(address z) public {
    address a = owner;
    if(a==a){
      a = z;
      require(msg.sender != owner);
      //self destruct(x==msg.sender) // sanitized so its ok
    }
    else{
      address b = owner;
    }

    // here arguments should not be trusted, one execution path has no sanitize
    // but x is not tainted
    selfdestruct(msg.sender);
  }
}
