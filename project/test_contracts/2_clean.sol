pragma solidity ^0.5.0;

contract Contract {
  address owner;
  address myaddressTest;
  function foo(address x) public {
    require(x == owner);
    myaddressTest = msg.sender;
    require(myaddressTest == owner);       // not a guard
    address mine = bar(x);
    address test = msg.sender;
    selfdestruct(msg.sender);   // vulnerable 
  }
  function bar(address x) public returns(address){
    require(msg.sender == owner);
    return x;
  }
}
